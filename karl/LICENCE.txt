Karl Nawrot's drawings are not published under copyleft licence, but CC BY-NC-ND (https://creativecommons.org/licenses/by-nc-nd/3.0/)
